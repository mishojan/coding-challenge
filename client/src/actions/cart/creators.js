import {
  HANDLE_SUBTRACT_ONE,
  HANDLE_ADD_ONE
} from './types';

/* Action creators */

export const handleSubtractOne = product => ({
  type: HANDLE_SUBTRACT_ONE,
  product,
});

export const handleAddOne = product => ({
  type: HANDLE_ADD_ONE,
  product,
});