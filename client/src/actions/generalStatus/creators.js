import {
  WEB_APP_ONLINE,
} from './types';

/* Action creators */

export const navigatorOnLine = () => ({
  type: WEB_APP_ONLINE,
  payload: navigator.onLine,
});