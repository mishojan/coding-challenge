import { FETCH_PRODUCTS } from './types';
import { getProducts } from '../../apis';

/* Action creators */

export const fetchProducts = page => async dispatch => {
  try {
    const response = await getProducts(page);

    dispatch({ type: FETCH_PRODUCTS, payload: response.data, page });
  } catch (error) {
    console.error(error);
  }
};