import axios from 'axios';

const API_BASE_URL = 'https://challenge-api.aerolab.co';

// Aerolab Challenge API
const aerolabApi = axios.create({
  baseURL: API_BASE_URL,
  headers: {
    'Content-Type': 'application/json',
  },
});

// Products instance
export const getProducts = page => aerolabApi.get(`/products?page=${page ? page : 1}`);