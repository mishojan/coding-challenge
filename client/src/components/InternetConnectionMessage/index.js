import React from 'react';
import PropTypes from 'prop-types';

export default function InternetConnectionMessage({ internetIsOnline }) {
  function createRender() {
    let onlineContent = <p>De nuevo en línea</p>;
    let offlineContent = <p>Sin conexión</p>;
    let content = null;
    let hideMessageAfterOn = false;

    if (internetIsOnline !== null) {
      if (internetIsOnline) {
        if (content === offlineContent) {
          hideMessageAfterOn = true;
        }
        content = onlineContent;
      } else {
        content = offlineContent;
      }

      return (
        <div className={`internet-connection-message ${hideMessageAfterOn ? 'hideOnAfter3Sec' : 'hideNow'} ${internetIsOnline ? 'on' : 'off'}`}>
          {content}
        </div>
      );
    } else {
      return null;
    }
  }

  return createRender();
}

InternetConnectionMessage.propTypes = {
  internetIsOnline: PropTypes.bool,
};