import React from 'react';
import PropTypes from 'prop-types';

export default function Counter({
  counter
}) {
  return (<div className="cart-items-counter">
    <span>{counter}</span>
  </div>);
}

Counter.propTypes = {
  counter: PropTypes.number.isRequired,
};