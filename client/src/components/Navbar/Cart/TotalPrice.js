import React from 'react';
import PropTypes from 'prop-types';

export default function TotalPrice({ totalPrice }) {
  return (
    <div className="cart-total-price">
      <p>${totalPrice}</p>
    </div>
  );
}

TotalPrice.propTypes = {
  totalPrice: PropTypes.any,
};