import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import CartImg from '../../../assets/images/shopping-cart.png';
import TotalPrice from './TotalPrice';
import Counter from './Counter';

function Cart({ products, totalPrice, totalProductsOnCart }) {
  return (
    <div className="cart">
      {products.length > 0 &&
        <TotalPrice
          totalPrice={totalPrice}
        />
      }
      <div className="cart-image">
        <img src={CartImg} alt="Cart" />
      </div>
      <Counter counter={totalProductsOnCart} />
    </div>
  );
}

Cart.propTypes = {
  products: PropTypes.array.isRequired,
  totalProductsOnCart: PropTypes.number.isRequired,
  totalPrice: PropTypes.number.isRequired,
};

const mapStateToProps = state => ({
  products: state.cart.products,
  totalProductsOnCart: state.cart.totalProducts,
  totalPrice: state.cart.totalPrice,
});

export default connect(
  mapStateToProps,
  null,
)(Cart);

