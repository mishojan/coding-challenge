import React from 'react';
import LogoImg from '../../../assets/images/Combined Shape.png';
import LogoImgBig from '../../../assets/images/Combined Shape 2@2x.png';
import LogoImgVeryBig from '../../../assets/images/Combined Shape 2@3x.png';

export default function Logo() {
  return (
    <div className="logo">
      <img src={LogoImg} srcSet={`${LogoImgVeryBig} 1x, ${LogoImgBig} 2x, ${LogoImgVeryBig} 3x`} alt="Logo" />
      <p>
        <span>Ez</span>shop
      </p>
    </div>
  );
}