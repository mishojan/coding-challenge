import React from 'react';
import Logo from './Logo';
import Cart from './Cart';

export default function Index() {
  return (
    <div className="navbar">
      <div className="navbar-container">
        <div className="navbar-content">
          <Logo />
          <Cart />
        </div>
      </div>
    </div>
  );
}