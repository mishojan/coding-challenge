import React from 'react';
import PropTypes from 'prop-types';
import CTAminus from '../../assets/images/CTA-small.png';

export default function CTA({
  handleAddToCart,
  handleSubtractOne,
  handleAddOne,
  quantity
}) {
  return (
    <div className="product-cta">
      <button type="button" className="add-to-cart-cta" onClick={handleAddToCart}>
        Agregar al carrito
      </button>
      <div className="quantity-ctas">
        <button type="button" className={`subtract-one-cta ${quantity === 0 ? 'disabled' : ''}`} onClick={handleSubtractOne} disabled={quantity === 0}>
          <img src={CTAminus} alt="Restar" />
        </button>
        <p>{quantity}</p>
        <button type="button" className="add-one-cta" onClick={handleAddOne}>
          <img src={CTAminus} alt="Sumar" />
        </button>
      </div>
    </div>
  );
}

CTA.propTypes = {
  handleAddToCart: PropTypes.func.isRequired,
  handleSubtractOne: PropTypes.func.isRequired,
  handleAddOne: PropTypes.func.isRequired,
  quantity: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};