import React from 'react';
import PropTypes from 'prop-types';

export default function Cost({
  discount,
  quantity,
  price
}) {
  return (
    <div className="product-cost">
      <p className="product-discount">{discount !== price ? `$${discount}` : null}</p>
      <p className="product-price">{quantity ? `${quantity}x` : null} ${price}</p>
    </div>
  );
}

Cost.propTypes = {
  discount: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  quantity: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};
