import React from 'react';
import PropTypes from 'prop-types';

export default function Description({
  description,
}) {
  return <p className="product-description">{description.length > 40 ? description.substring(0, 40) + '...' : description}</p>;
}

Description.propTypes = {
  description: PropTypes.string.isRequired,
};