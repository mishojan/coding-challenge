import React from 'react';
import PropTypes from 'prop-types';
import Skeleton from '../Skeleton';

export default function handleImage({ img }) {
  function handleImage() {
    if (img) {
      return <img src={img} alt="" />;
    } else {
      return <Skeleton />;
    }
  }

  return (
    <div>
      {handleImage()}
    </div>
  );
}

Image.propTypes = {
  img: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
};