import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Cost from './Cost';
import CTA from './CTA';
import Description from './Description';
import { handleSubtractOne, handleAddOne } from '../../actions/cart/creators';
import Skeleton from '../Skeleton';
import Image from './Image';

class Product extends React.Component {
  handleSubtractOne = product => {
    this.props.handleSubtractOne(product);
  };

  handleAddOne = product => {
    this.props.handleAddOne(product);
  };

  renderImage = (internetIsOnline, photo) => {
    if (internetIsOnline && photo) {
      return <Image img={photo}/>;
    } else if (!internetIsOnline && photo) {
      return <Image img={photo}/>;
    } else {
      return <Skeleton />;
    }
  };

  render() {
    const { name, price, photo, originalPrice } = this.props.productInfo;
    const { quantity, internetIsOnline } = this.props;

    return (
      <div className={`product-card ${quantity > 0 ? 'active' : ''}`}>
        <React.Suspense fallback={<Skeleton />}>
          {this.renderImage(internetIsOnline, photo)}
          <Description description={name} />
          <Cost
            discount={originalPrice}
            price={price}
            quantity={quantity}
          />
          <CTA
            handleAddToCart={() => this.handleAddOne(this.props.productInfo)}
            handleSubtractOne={() => this.handleSubtractOne(this.props.productInfo)}
            handleAddOne={() => this.handleAddOne(this.props.productInfo)}
            quantity={quantity}
          />
        </React.Suspense>
      </div>
    );
  }
}

Product.propTypes = {
  internetIsOnline: PropTypes.bool,
  productInfo: PropTypes.object.isRequired,
  quantity: PropTypes.number,
};

const mapStateToProps = state => ({
  internetIsOnline: state.generalStatus.navigatorOnLine,
});

const mapDispatchToProps = dispatch => ({
  handleSubtractOne: product => dispatch(handleSubtractOne(product)),
  handleAddOne: product => dispatch(handleAddOne(product)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Product);