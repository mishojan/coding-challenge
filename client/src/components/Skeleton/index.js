import React from 'react';

export default function Skeleton() {
  return (
    <div className="skeleton">
      <div className="line1" />
      <div className="line2" />
      <div className="line3" />
      <div className="line4" />
    </div>
  )
};