import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import { fetchProducts } from '../actions/products/creators';
import Container from '../components/Container';
import Skeleton from '../components/Skeleton';
import Product from '../components/Product';

class Home extends React.Component {
  state = {
    internetIsOnline: this.props.internetIsOnline,
  };

  componentDidMount() {
    if (this.state.internetIsOnline) {
      if (!this.props.pagination.page) {
        this.props.fetchProducts();
      }
    }
  };

  componentWillReceiveProps(nextProps, nextContext) {
    if (this.props.internetIsOnline !== nextProps.internetIsOnline) {

      if (this.props.internetIsOnline !== true && nextProps.internetIsOnline) {
        if (!this.props.pagination.page) {
          this.props.fetchProducts();
        } else {
          if (!JSON.parse(localStorage.getItem('products'))) {
            for (let i = 0; i <= this.props.pagination.page; i ++) {
              this.props.fetchProducts(i);
            }
          }
        }
      }

      this.setState({
        internetIsOnline: nextProps.internetIsOnline,
      });
    }
  }

  renderProducts = () => {
    return this.props.productsCatalog.map(product => (
      <Product
        key={product.id}
        quantity={this.props.productsOnCart.find(p => p.id === product.id) !== undefined ? this.props.productsOnCart.find(p => p.id === product.id).quantity : 0}
        productInfo={product}
      />
    ));
  };

  loadMore = () => {
    if (this.state.internetIsOnline) {
      if (!this.props.pagination.page) {
          this.props.fetchProducts();
      } else {
        if (this.props.pagination.page < this.props.pagination.page_count)
          this.props.fetchProducts(this.props.pagination.page + 1);
      }
    }
  };

  render() {
    return (
      <div id="home-page">
        <Container>
          <h1 className="product-title">Almacén</h1>
          <React.Suspense fallback={<Skeleton />}>
            <div className="product-list">
              <InfiniteScroll
                className="infinite-scroll"
                pageStart={0}
                hasMore={this.props.pagination.page < this.props.pagination.page_count}
                loadMore={this.loadMore}
                loader={<Skeleton key={Math.random()} />}
              >
                {this.renderProducts()}
              </InfiniteScroll>
            </div>
          </React.Suspense>
        </Container>
      </div>
    );
  }
}

Home.propTypes = {
  internetIsOnline: PropTypes.bool,
  fetchProducts: PropTypes.func.isRequired,
  productsCatalog: PropTypes.array.isRequired,
  productsOnCart: PropTypes.array.isRequired,
  totalProductsOnCart: PropTypes.number.isRequired,
  pagination: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  internetIsOnline: state.generalStatus.navigatorOnLine,
  productsCatalog: state.products.products,
  productsOnCart: state.cart.products,
  totalProductsOnCart: state.cart.totalProducts,
  pagination: state.products.pagination,
});

const mapDispatchToProps = dispatch => ({
  fetchProducts: page => dispatch(fetchProducts(page)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);