import {
  HANDLE_SUBTRACT_ONE,
  HANDLE_ADD_ONE,
} from '../../actions/cart/types';
import _ from 'lodash';

const cartStatusTree = {
  products: loadProductsFromLocalStorage(),
  totalProducts: loadTotalProductsFromLocalStorage(),
  totalPrice: loadTotalPriceFromProductsLocalStorage(),
};

export const cart = (prevState = cartStatusTree, action) => {
  let findProductOnProducts = action.product;

  if (findProductOnProducts) {
    if (prevState.products.length > 0) {
      if (_.find(prevState.products, product => product.id === findProductOnProducts.id) !== undefined) {
        findProductOnProducts = _.find(prevState.products, product => product.id === findProductOnProducts.id);
      }
    }
  }
  switch (action.type) {
    case HANDLE_SUBTRACT_ONE:
      return {
        ...prevState,
        ...subtractOneFromProducts(prevState.products, prevState.totalProducts, prevState.totalPrice, findProductOnProducts),
      };
    case HANDLE_ADD_ONE:
      return {
        ...prevState,
        ...addOneToProducts(prevState.products, prevState.totalProducts, prevState.totalPrice, findProductOnProducts),
      };
    default:
      return prevState;
  }
};

function subtractOneFromProducts(products, totalProducts, totalPrice, findProductOnProducts) {
  const result = {
    products: null,
    totalProducts: null,
  };

  if (findProductOnProducts.quantity === 1) { // If is left 1 product just deleted from the array
    result.products = _.filter(products, product => product.id !== findProductOnProducts.id);
  } else {
    result.products = _.uniqBy(
      [...products, { ...findProductOnProducts, quantity: findProductOnProducts.quantity-- }],
      'id'
    );
  }

  result.totalProducts = totalProducts - 1;
  result.totalPrice = parseFloat((totalPrice - findProductOnProducts.price).toFixed('2'));

  saveOnLocalStorage(result);


  return result;
}

function addOneToProducts(products, totalProducts, totalPrice, findProductOnProducts) {
  const result = {
    products: _.uniqBy(
      [...products, findProductOnProducts.quantity ? { ...findProductOnProducts, quantity: findProductOnProducts.quantity++ } : { ...findProductOnProducts, quantity: 1 } ],
      'id'),
    totalProducts: totalProducts + 1,
    totalPrice: parseFloat((totalPrice + findProductOnProducts.price).toFixed('2')),
  };

  saveOnLocalStorage(result);

  return result;
}

function saveOnLocalStorage(value) {
  localStorage.setItem('cart', JSON.stringify(value));
}

function loadProductsFromLocalStorage() {
  if (JSON.parse(localStorage.getItem('cart'))) {
    if (JSON.parse(localStorage.getItem('cart')).products) {
      return JSON.parse(localStorage.getItem('cart')).products;
    } else {
      return [];
    }
  } else {
    return [];
  }
}

function loadTotalProductsFromLocalStorage() {
  if (JSON.parse(localStorage.getItem('cart'))) {
    if (JSON.parse(localStorage.getItem('cart')).totalProducts) {
      return JSON.parse(localStorage.getItem('cart')).totalProducts;
    } else {
      return 0;
    }
  } else {
    return 0;
  }
}

function loadTotalPriceFromProductsLocalStorage() {
  const products = loadProductsFromLocalStorage();
  let totalPrice = 0;

  if (products) {
    products.forEach(product => {
      totalPrice = totalPrice + (product.price * product.quantity);
    });

    localStorage.setItem('cart', JSON.stringify({ ...JSON.parse(localStorage.getItem('cart')), totalPrice }));
  }

  return parseFloat((totalPrice).toFixed('2'));
}