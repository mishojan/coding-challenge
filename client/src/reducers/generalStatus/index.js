import {
  WEB_APP_ONLINE,
} from '../../actions/generalStatus/types';

const generalStatusTree = {
  navigatorOnLine: null,
};

export const generalStatus = (prevState = generalStatusTree, action) => {
  switch (action.type) {
    case WEB_APP_ONLINE:
      return {
        ...prevState,
        navigatorOnLine: action.payload,
      };
    default:
      return prevState;
  }
};