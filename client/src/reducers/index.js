import { combineReducers } from 'redux';
import { generalStatus } from './generalStatus';
import { cart } from './cart';
import { products } from './products';

export default combineReducers({
  generalStatus,
  cart,
  products,
});