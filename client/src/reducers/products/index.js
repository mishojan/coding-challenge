import { FETCH_PRODUCTS } from '../../actions/products/types';

const productsStatusTree = {
  products: loadProductsFromLocalStorage(),
  pagination: {
    ...loadPaginationFromLocalStorage(),
  },
};

export const products = (prevState = productsStatusTree, action) => {
  switch (action.type) {
    case FETCH_PRODUCTS:
      savePaginationToLocalStorage(action.payload.page, action.payload.per_page, action.payload.page_count);
      saveProductsToLocalStorage(action.payload.products);

      return {
        ...prevState,
        products: [...prevState.products, ...action.payload.products],
        pagination: {
          ...prevState.pagination,
          page: action.payload.page,
          per_page: action.payload.per_page,
          page_count: action.payload.page_count,
        },
      };
    default:
      return prevState;
  }
};

function loadProductsFromLocalStorage() {
  return JSON.parse(localStorage.getItem('products')) ? JSON.parse(localStorage.getItem('products')) : [];
}

function loadPaginationFromLocalStorage() {
  return JSON.parse(localStorage.getItem('pagination')) ? JSON.parse(localStorage.getItem('pagination')) : {};
}

function saveProductsToLocalStorage(products) {
  if (Object.entries(loadPaginationFromLocalStorage()).length > 0) {
    localStorage.setItem('products', JSON.stringify([...loadProductsFromLocalStorage(), ...products]));
  } else {
    localStorage.setItem('products', JSON.stringify(products));
  }
}

function savePaginationToLocalStorage(page, per_page, page_count) {
  if (Object.entries(loadPaginationFromLocalStorage()).length > 0) {
    if (page > JSON.parse(localStorage.getItem('pagination')).page) {
      localStorage.setItem('pagination', JSON.stringify({...loadPaginationFromLocalStorage(), page, per_page, page_count }));
    }
  } else {
    localStorage.setItem('pagination', JSON.stringify({ page, per_page, page_count }));
  }
}