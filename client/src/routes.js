import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import './assets/scss/main.scss';
import { navigatorOnLine } from '../src/actions/generalStatus/creators';
import HomePage from './pages/Home';
import Navbar from './components/Navbar';
import InternetConnectionMessage from './components/InternetConnectionMessage';



class Routes extends React.Component {
  componentDidMount() {
    window.addEventListener('load', () => {
      // 1st, we set the correct status when the page loads
      this.props.navigatorOnLine();

      // now we listen for network status changes
      window.addEventListener('online', () => {
        this.props.navigatorOnLine();

      });

      window.addEventListener('offline', () => {
        this.props.navigatorOnLine();

      });
    });
  }

  render() {
    return (
      <BrowserRouter>
        <Navbar />
        <InternetConnectionMessage internetIsOnline={this.props.internetIsOnline} />
        {/* Home page path */}
        <Route path="/" exact component={HomePage} />
      </BrowserRouter>
    );
  }
}

const mapStateToProps = (state) => ({
  internetIsOnline: state.generalStatus.navigatorOnLine,
});

const mapDispatchToProps = dispatch => ({
  navigatorOnLine: () => dispatch(navigatorOnLine()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Routes);