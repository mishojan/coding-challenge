## Welcome to my Coding challenge API

In the project directory, you can run:

### `npm run dev`

Runs the app in the development mode.<br>
Open [http://localhost:3001](http://localhost:3001) to view it in the browser.

The page will reload if you make edits.<br>

### `npm start`

Runs the app but doesn't watch with nodemon.<br>
Its purpose is used with `now` to deploy.

### `now`

For a staging deployment


### `now --target production`
For a production deployment


More information about using `now` on [https://zeit.co/examples/express](https://zeit.co/examples/express).