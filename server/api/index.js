const express = require('express');
const responseTime = require('response-time');
const products = require('./products');
const slowProducts = require('./slow-products');
const app = express();

app.set('port', 3001);

// Create a middleware that adds a X-Response-Time header to responses.
app.use(responseTime());
app.use(express.json());

app.use('/products', products);
app.use('/slow/products', slowProducts);

app.listen(app.get("port"), () => console.log('Server listening on port:', app.get("port")));

module.exports = app;