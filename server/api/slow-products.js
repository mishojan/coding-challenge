const express = require('express');
const axios = require('axios');
const moment = require('moment');
// TODO: Redis deactivate
// const redis = require('redis');
// const client = redis.createClient();
const { productsSlowApi, dollarApi } = require('../utils');
const router = express.Router();

// Key to cache products
// TODO: Redis deactivate
// const keyCacheProducts = 'products';
// const keyCacheProductsPage = 'productPage';

const getProducts = async (req, res) => {
  const { page } = req.query;
  let getDollarApi = await axios.get(dollarApi).then(res => res.data);
  let modifiedProducts = null;

  try {
    if (req.query && page) {
      await axios.get(`${productsSlowApi}?page=${page}`)
        .then(res => {
          console.log(`Cotizacion del dolar: ${getDollarApi.rate} ${getDollarApi.currency}`);

          modifiedProducts = {
            ...apiResponse.data,
            products: modifyProducts(apiResponse.data.products, getDollarApi.rate),
            per_page: apiResponse.data.per_page,
            page_count: apiResponse.data.page_count,
          };

          // TODO: Redis deactivate
          // client.set(`${keyCacheProductsPage}${page}`, JSON.stringify(modifiedProducts));

          res.status(200).json(modifiedProducts);
        })
        .catch(error => {
          if (error.response.data.error) {
            // TODO: Redis deactivate
            // client.set(`${keyCacheProductsPage}${page}`, JSON.stringify({
            //   error: "Así que usted tambien anda probando con páginas que no existen. Casi hacker.",
            //   url: "https://www.youtube.com/watch?v=gw_bATX4T1M"
            // }));

            res.status(400).json({
              error: "Así que usted tambien anda probando con páginas que no existen. Casi hacker.",
              url: "https://www.youtube.com/watch?v=gw_bATX4T1M"
            });
          } else {
            throw new Error();
          }
        });
    } else {
      console.log(`Cotizacion del dolar: ${getDollarApi.rate} ${getDollarApi.currency}`);

      await axios.get(productsSlowApi)
        .then(apiResponse => {

          modifiedProducts = {
            ...apiResponse.data,
            products: modifyProducts(apiResponse.data.products, getDollarApi.rate),
            per_page: apiResponse.data.per_page,
            page_count: apiResponse.data.page_count,
          };

          // TODO: Redis deactivate
          // client.set(keyCacheProducts, JSON.stringify(modifiedProducts));

          res.status(200).json(modifiedProducts);
        })
        .catch(error => {
          throw new Error();
        });
    }
  } catch (err) {
    res.status(400).json({ error: 'Parece que surgio un error inesperado. Lamentamos las molestias.' });
  }
};

// TODO: Redis deactivate
// function getSlowProductsCache(req, res) {
//   // Check the cache data from the server redis
//   if (req.query && req.query.page) {
//     client.get(`${keyCacheProductsPage}${req.query.page}`, (err, result) => {
//       if (result) {
//         if (JSON.parse(result) !== null) {
//           if (JSON.parse(result).error) {
//             res.status(400).send(JSON.parse(result))
//           } else {
//             res.status(200).send(JSON.parse(result));
//           }
//         } else {
//           getProducts(req, res)
//         }
//       } else {
//         getProducts(req, res);
//       }
//     });
//   } else {
//     client.get(keyCacheProducts, (err, result) => {
//       if (result) {
//         res.status(200).send(JSON.parse(result));
//       } else {
//         getProducts(req, res);
//       }
//     });
//   }
// }

function modifyProducts(products, dollarRate) {
  let countOldProducts = 0;

  const modifiedProducts = products.map(product => {
    if (!(moment().diff(moment(product.updatedAt), 'months') < 1)) {
      countOldProducts += 1;

      return null;
    }

    return {
      ...product,
      dollarPrice: parseFloat((product.price / dollarRate).toFixed(2))
    };
  }).filter(product => product !== null);

  console.log(`Productos viejos eliminados: ${countOldProducts} de ${products.length}`);

  return modifiedProducts;
}

router.get('/', getProducts);

module.exports = router;
