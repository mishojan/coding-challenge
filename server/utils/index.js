// Apis
const productsApi = 'https://challenge-api.aerolab.co/products';
const dollarApi = 'https://challenge-api.aerolab.co/dollar';
const productsSlowApi = 'https://challenge-api.aerolab.co/slow/products';

module.exports = {
  productsApi,
  productsSlowApi,
  dollarApi
};
